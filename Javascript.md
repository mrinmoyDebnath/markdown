# Basic Data Types and Basic Data Structures in JavaScripts

## Data Types

- Data type is defined by the values a particular kind of data item can take.
- To be able to operate on variables, it is important to know about the type of data.
- JavaScript is a dynamic type language (type is interpreted as and when the data is encountered).
- Two types of data types:
  - Primitive data type
  - Non - Primitive data type

## Var, Let and const

### Var

- Used to declare variables.
- Scope of the variable is global inside a block.
- Cannot be accessed outside block.
- Can be re-declared and updated.
- Wrong use can lead to major errors.

### let

- Variables are block scoped.
- Variables declared within a block is available to that block only.
- Can be updated but not re-declared.
- Declaration is possible inside a different scope.

### const

- Variable value cannot change once declared.
- Values are block scoped.
- Cannot be updated or re-declared

## Primitive Data Types

Simple data types. Variables hold single value.

### Strings

- It is a sequence of characters.
- Usually used to contain text data.
- Written inside either of the single or double quotes.

``` javascript
    let bike = 'Ducati';
    let car = "Audi";
    let letter = "a";
```
- Some String utility methods and property-
```javascript
    var str = 'This is a string. This is written inside quotes'
    //To find length of string
    str.length
    //To find the index of first occurrence of a text
    str.indexOf('is') //5
    //To find the index of last occurrence of a text
    str.lastIndexOf('is') // 23
    //To replace words or letters with another
    str.replace('string', 'text')
    //To convert string to uppercase
    str.toUpperCase()
    //To convert string to lowercase
    str.toLowerCase()
    //To join string str2 with str1
    str1.concat(str2)
    //Remove whitespaces from the left and right of the text
    str.trim()
    //To extract part of a string
    str.slice(beginningIndex, endIndex)

```
### Booleans

- Can be either true or false.

``` javascript
    let flag1 = true;
    let flag2 = false;
    let flag3 = !true;  //evaluates to not true i.e. false
```

### Numbers

- Numbers can be integers or decimals.
- Numbers can be written using scientific notation.

``` javascript
    let no1 = 12;
    let no2 = 32.45;
    let no3 = 12e-6;    // 0.000012
    let no4 = 12e6;     // 12000000 
```
- Some number utility methods - 
```javascript
    let no = 12.32;
    //To get number as a string
    no.toString()
    //To get Integer from a string
    let str = '5.2';
    Number.parseInt(str)
    // To get either integer or float from string 
    Number.parseFloat(str)
    //To check if the element is an integer 
    Number.isInteger(str)
    //To check if the element is Not a Number
    Number.isNaN(str)
    
```

### Undefined

- Represents undefined values.

``` javascript
    var x;  // value not assigned.
```

## Non - Primitive (Complex) Data Types

Can be seen as a collection of simple data types. Can hold more than one value.

### Object

- Represents instance through which we can access members.
- Key value pair.
- Can contain functions, array, simple data, objects as value.
- Pairs are written inside curly braces.
- Values are assigned to pairs using ':'.
- More pairs can be added using comma separation.

``` javascript
    var student = {
        firstName: "Abc",
        secondName: "Xyz",
        id: 123,
        hobbies: [
            "Gaming",
            "Singing",
            "Dancing"
        ]
    }
    console.log(student['firstName']);   // 'Abc'
    console.log(student.firstName);      // 'Abc'
```
- Some Utility Methods - 
```javascript
    //To get all object keys 
    Object.keys(obj)
    //To get all object values
    Object.values(obj)
    //To copy all elements from obj2 to obj1
    Object.assign(obj1, obj2)
    //To create obj1 from list l1 of [key, value] pairs
    obj = Object.fromEntries(l1)
    //To create an object with specified properties and values
    const person = {
        isHuman: false,
        printIntroduction: function() {
            console.log(`My name is ${this.name}. Am I human? ${this.isHuman}`);
        }
    };

    const me = Object.create(person);
```
### List

- Represents a collection of data.
- Any type of values can be added to the list.
- It can be called an array if the list contains all the datas with same type.
- The values are indexed and hence can be accessed by their index.

``` javascript
    const array = [1, 2, 3, 4, 5];
    let list = [
        'helmet',
        'burger',
        {classes: false},
        22,
        33
    ]
```
- Some List Utility methods and property- 
```javascript
    //To find length of a list
    li.length
    //To append new data to l1
    l1.append(12)
    //To remove last data from l1
    l1.pop()
    //To remove first element from l1
    l1.shift()
    //Add an item to the front of list l1
    l1.unshift('abc')
    //Find an index of element
    li.indexOf('abc')
    //To remove an item from an index
    li.splice(index, 1)
    //To remove n items from after the index
    li.splice(index, n)
```

### Null

- Represents no value.
- It is an Object which represents absence of value.

``` javascript
    const house = null;
```
### **To check data type of an element :**
```javascript
    const abc = 'This is String';
    console.log(typeof abc);
```
## Data Structures

- It is a way or organizing data.
- Used to manipulate/use data in an efficient manner.

### Stack

- List of data.
- New data is added to top of the list. This is called push operation.
- Data is always taken from the top of the list. This is called pop operation.
- Follows LIFO structure.

``` javascript
    //Stack initialization
    const stack = [ 43, 'abc', 90.32, true];
    console.log('initial stack: ',stack);


    //push operation
    stack.push(12);
    stack.push('text');
    stack.push(3 ,false);
    const arr2 = ['a', 'b'];
    stack.push(arr2);
    console.log('after push: ',stack);


    //pop operation
    const x = stack.pop()
    console.log('after pop: ',stack);
    console.log('returned value: ', x);
```

### Queue

- List of data.
- New data is added to the last of the list. This is called enqueue operation.
- Data are processed from the beginning of the list. This is dequeue operation.
- Follows FIFO structure.

``` javascript
    //Queue initialization
    const Queue = [1, 'a', true, 1.1];
    console.log('Initial Queue: ',Queue);

    //enqueue operation
    Queue.push(32, 'avc');
    console.log('After enqueue: ',Queue);

    //dequeue operation
    const x = Queue.shift() 
    console.log('After dequeue', Queue);
```

### Hash Table
- Data stored in associative manner.
- Data stored in an array format.
- Each data has its own unique value.
- Very fast to perform insertion and search operation.
- Location of a data is determined by a hash function.
- Can be implemented in a number of ways according to requirements.

```javascript
    hashTable[hashIndex] = value;

```

**More complex data structures could be made using a combination of these data structures using different data types. Some of these data structures are: -**

*[Tree](https://www.tutorialspoint.com/data_structures_algorithms/tree_data_structure.htm)
*[Graph](https://www.geeksforgeeks.org/graph-data-structure-and-algorithms/)